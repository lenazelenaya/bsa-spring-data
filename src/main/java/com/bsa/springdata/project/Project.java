package com.bsa.springdata.project;

import com.bsa.springdata.office.Office;
import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.Technology;
import com.bsa.springdata.user.User;
import com.bsa.springdata.user.dto.CreateUserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

// TODO: Map table projects to this entity
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Team> teams;

//    public static Project fromDto(CreateProjectRequestDto projectRequestDto, List<Team> team) {
//        return Project.builder()
//                .name(projectRequestDto.getProjectName())
//                .description(projectRequestDto.getProjectDescription())
//                .teams(team)
//                .build();
//    }
}

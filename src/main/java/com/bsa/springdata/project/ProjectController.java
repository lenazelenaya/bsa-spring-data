package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/project/")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/biggest")
    public Optional<ProjectDto> getTheBiggest() {
        return projectService.findTheBiggest();
    }

    @GetMapping("/technology")
    public List<ProjectDto> findTop5ByTechnology(@RequestParam(defaultValue = "Java") String technology) {
        return projectService.findTop5ByTechnology(technology);
    }

    @GetMapping("/role")
    public int getCountByRole(@RequestParam(defaultValue = "Developer") String role){
        return projectService.getCountWithRole(role);
    }

    @GetMapping("/info")
    public List<ProjectSummaryDto> getSummary(){
        return projectService.getSummary();
    }

    @PostMapping("/create")
    public UUID createNewProj(@RequestBody CreateProjectRequestDto createProjectRequestDto){
        return projectService.createWithTeamAndTechnology(createProjectRequestDto);
    }
}

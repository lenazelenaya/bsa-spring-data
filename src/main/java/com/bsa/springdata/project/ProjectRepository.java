package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;
import java.util.List;
import java.util.UUID;

@Repository
public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query(value = "select p.* from projects p, teams t, technologies tech, " +
            "users u where tech.name = :technology" +
            " group by p.id" +
            " order by count(u.id) desc",
            nativeQuery = true)
    List<Project> findTop5ByTechnology(String technology, Pageable pageable);

    @Query(value = "select p.* from projects p, teams t ,users u " +
            "group by p.id, p.name " +
            "order by  count(distinct t.id) desc, count(distinct u.id) desc, p.name desc limit 1",
            nativeQuery = true)
    List<Project> findTopByTeamsDevsName();

    @Query("select count(distinct p.id) from Project p, Role r " +
            "where r.name = :role")
    int getCountWithRole(String role);


    @Query(value="select p.name as name, count(distinct u.id) as developersNumber, " +
            "count(distinct t.id) as teamsNumber, " +
            "array_to_string(array_agg(distinct tech.name order by tech.name desc), ', ') as technologies " +
            "from projects p, teams t, technologies tech, users u " +
            "where t.project_id=p.id and t.id=u.team_id " +
            "group by p.name order by p.name",
        nativeQuery=true
    )
    List<ProjectSummaryDto> getSummary();
}
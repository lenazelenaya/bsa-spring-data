package com.bsa.springdata.role;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends JpaRepository<Role, UUID> {

    @Modifying
//    @Query( value = "delete from roles r " +
//            "where r.name = ?1 and r.id in (select role_id " +
//            "from user2role u2 where u2.user_id IS NULL)",
//    nativeQuery = true
//    )

    @Query("delete from Role r where size(r.users)=0")
    void deleteByName(String roleCode);
}

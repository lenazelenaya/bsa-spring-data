package com.bsa.springdata.team;

import com.bsa.springdata.project.Project;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.team.dto.TechnologyDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Modifying
    @Query(
            value = "update teams t set technology_id = ( select tech.id from technologies tech where tech.name = ?3)" +
                    " where technology_id = (select tech.id from technologies tech where tech.name = ?2)" +
                    " and (select count(*) from users u where u.team_id=t.id)<?1",
            nativeQuery = true
    )
    void updateTechnology(int devsNumber,
                          String oldTechnologyName, String newTechnologyName);


    Optional<Team> findByName(String teamName);

    int countByTechnologyName(String technology);

    @Modifying
    @Query(
            value = "update teams " +
                    "set name = (select (t.name || '_' || p.name || '_' || tech.name) " +
                    "from teams t, projects p, technologies tech " +
                    "where p.id=t.project_id and tech.id=t.technology_id and t.name = ?1)" +
                    "where name = ?1",
            nativeQuery = true
    )
    void normalizeName(String hipsters);
}

package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/team/")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @PostMapping("/normalize")
    public void normalizeTeam(@RequestParam(defaultValue = "Hipsters") String teamName){
        teamService.normalizeName(teamName);
    }

    @PostMapping("/updateTech")
    public void updateTech(@RequestParam(defaultValue = "5") int devCount,
                           @RequestParam(defaultValue = "JavaScript") String oldTechnology,
                           @RequestParam(defaultValue = "Java") String newTechnology){
        teamService.updateTechnology(devCount, oldTechnology, newTechnology);
    }
}

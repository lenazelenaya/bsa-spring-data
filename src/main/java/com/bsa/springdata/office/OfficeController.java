package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/office/")
public class OfficeController {
    @Autowired
    OfficeService officeService;

    @GetMapping("/technology")
    public List<OfficeDto> findByTechnology(@RequestParam(defaultValue = "Java") String technology){
        return officeService.getByTechnology(technology);
    }

    @PostMapping("/updateAdress")
    public Optional<OfficeDto> updateAdress(@RequestParam(defaultValue = "Chornovola 59") String oldAdress,
                                            @RequestParam(defaultValue = "Chornovola 31") String newAdress){
        return officeService.updateAddress(oldAdress, newAdress);
    }
}

package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("select distinct o from Office o, Technology tech where tech.name= :technology")
    List<Office> findByTechnology(@Param("technology")String technology);

    @Modifying
    @Query(
            value = "update offices o set address = ?2 " +
                    "from users u " +
                    "where o.address = ?1 and u.office_id is not null ",
            nativeQuery = true
    )
    void updateAddress(String oldAddress, String newAddress);

    Office findByAddress(String address);
}

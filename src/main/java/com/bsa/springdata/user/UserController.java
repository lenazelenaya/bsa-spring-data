package com.bsa.springdata.user;

import com.bsa.springdata.user.dto.CreateUserDto;
import com.bsa.springdata.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDto> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/{id}")
    public UserDto getUser(@PathVariable UUID id) {
        return userService.getUserById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
    }

    @PostMapping
    public UUID createUser(@RequestBody CreateUserDto user) {
        return userService.createUser(user)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can not create user."));
    }

    @GetMapping("/city")
    public List<UserDto> getUsersByCity(@RequestParam(defaultValue = "lviv") String city) {
        return userService.findByCity(city);
    }

    @GetMapping("/lastName")
    public List<UserDto> getUsersByLastName(@RequestParam(defaultValue = "Dalton") String lastName,
                                            @RequestParam(defaultValue = "0") int page,
                                            @RequestParam(defaultValue = "1") int size) {
        return userService.findByLastName(lastName, page, size);
    }

    @GetMapping("/experience")
    public List<UserDto> getUsersByExperience(@RequestParam(defaultValue = "5") int experience) {
        return userService.findByExperience(experience);
    }

    @GetMapping("/location")
    public List<UserDto> getUsersByRoomAndCity(@RequestParam(defaultValue = "red") String room,
                                               @RequestParam(defaultValue = "Kiyv") String city) {
        return userService.findByRoomAndCity(city, room);
    }

    @DeleteMapping("/experience")
    public int deleteUsersByExperience(@RequestParam(defaultValue = "5") int experience){
        return userService.deleteByExperience(experience);
    }
}
